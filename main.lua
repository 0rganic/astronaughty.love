local FPS = require "libraries.fps"

function love.load()
	FPS.framerate = 60
	Object = require "libraries.class"
	Tick = require "libraries.tick"
	require "scripts.sound_manager"
	require "scripts.background"
	require "scripts.status_bar"
	require "scripts.black_hole"
	require "scripts.player"
	require "scripts.meteoroid"
	require "scripts.heart"

	soundManager = SoundManager()
	background = Background()
	statusBar = StatusBar()
	blackHole = BlackHole()
	player = Player()
	listOfMeteoroids = {}
	listOfHearts = {}

	Tick.recur(function() table.insert(listOfMeteoroids, Meteoroid()) end, math.random(0.1, 0.75))
	Tick.recur(function() table.insert(listOfMeteoroids, Meteoroid()) end, math.random(0.1, 0.75))
end

function love.keypressed(key)
	player:keyPressed(key)
	soundManager:keyPressed(key)
end

function love.update(dt)
	Tick.update(dt)
	statusBar:update(dt, player)
	blackHole:update(dt)
	player:update(dt)

	for index, meteoroid in ipairs(listOfMeteoroids) do
		meteoroid:update(dt)

		for index, meteoroid in ipairs(listOfMeteoroids) do
			meteoroid:checkCollision(player, soundManager)
			meteoroid:selfDestruct(meteoroid, blackHole)
		end

		if meteoroid.collide then
			table.remove(listOfMeteoroids, index)
		end

		if meteoroid.destruct then
			table.remove(listOfMeteoroids, index)
		end
	end


	for index, heart in ipairs(listOfHearts) do
		heart:update(dt)

		for index, meteoroid in ipairs(listOfMeteoroids) do
			heart:checkCollision(meteoroid, soundManager)
			heart:selfDestruct(heart)
		end

		if heart.collide then
			table.remove(listOfHearts, index)
		end

		if heart.destruct then
			table.remove(listOfHearts, index)
		end
	end

	if player.health <= 0 or statusBar.score >= statusBar.maxScore then
		Tick.delay(function() love.timer.sleep(2) end, 1)
		Tick.delay(function() love.event.quit() end, 1)
	end
end

function love.draw()
	background:draw()
	blackHole:draw()
	player:draw()
	statusBar:draw(player)

	for index, value in ipairs(listOfMeteoroids) do
		value:draw()
	end

	for index, value in ipairs(listOfHearts) do
		value:draw()
	end
end