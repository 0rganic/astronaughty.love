# Astronaughty

![Astronaughty's cover image](sprites/cover.png "Cover Art")

Run as far as you can from the emerging black hole in this love oriented space arcade shooter.

My project entry for Ludum Dare 42, the Compo.






# Windows

1. Download [this](https://gitlab.com/0rganic/astronaughty.love/blob/master/distribution/windows/astronaughty.zip "For Windows") file

2. Extract the ZIP file

3. Run "astronaughty.exe"

# Linux

1. Download [this](https://gitlab.com/0rganic/astronaughty.love/blob/master/distribution/linux/astronaughty.love "For Linux") file

2. Open the terminal in the directory where the file is located

3. Enter the following command in the terminal: ```love astronaughty.love```

# macOS

* Download [this](https://gitlab.com/0rganic/astronaughty.love/blob/master/distribution/macOS/astronaughty.zip "For macOS") file...rest I'm not so sure due to lack of knowledge...but I tried to follow the official steps to package the game

I don't have a lot of experience with macOS, so I think you should read the official guide yourself

* Follow the instructions: https://love2d.org/wiki/Game_Distribution#Creating_a_Mac_OS_X_Application

If you want to follow the process yourself, you can use the [Linux](https://gitlab.com/0rganic/astronaughty.love/blob/master/distribution/linux/astronaughty.love) downloadable file to make the macOS application yourself






# Controls:

* "Space" to send love

* "E" to spray hearts

* "M" to mute/unmute the game

* "Escape" to quit the game

# Libraries used:

* ["class"](https://github.com/rxi/classic "GitHub repository and its author")

* ["fps"](https://github.com/bjornbytes/tick "GitHub repository and its author")

* ["tick"](https://github.com/rxi/tick "GitHub repository and its author")

# Tools used:

* [LÖVE as a game framework to build the game](https://love2d.org "LÖVE's homepage")

* [Krita for art](https://krita.org "Krita's homepage")

* [Piconica to make music/sfx](https://play.google.com/store/apps/details?id=com.klikki.lab.picopico "Piconica's Playstore domain")

* [Audacity to edit music/sfx](https://www.audacityteam.org "Audacity's homepage")