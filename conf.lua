function love.conf(game)
	game.version = "11.1" -- LÖVE 11.1 (Mysterious Mysteries)

	game.window.title = "Astronaughty"
	game.window.icon = "sprites/heart.png"
	game.window.width = 1024
	game.window.height = 512
	game.window.borderless = false
	game.window.resizable = false
	game.window.minwidth = 1
	game.window.minheight = 1
	game.window.fullscreen = false
	game.window.fullscreentype = "desktop"

	game.modules.audio = true
	game.modules.data = false
	game.modules.event = true
	game.modules.font = true
	game.modules.graphics = true
	game.modules.image = true
	game.modules.joystick = false
	game.modules.keyboard = true
	game.modules.math = true
	game.modules.mouse = false
	game.modules.physics = false
	game.modules.sound = true
	game.modules.system = true
	game.modules.thread = true
	game.modules.timer = true
	game.modules.touch = false
	game.modules.video = false
	game.modules.window = true
end