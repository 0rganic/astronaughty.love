BlackHole = Object:extend()

function BlackHole:new()
	self.x = -176
	self.y = 0
	self.frames = {}
	self.currentFrame = 1

	for index = 1,10 do
		table.insert(self.frames, love.graphics.newImage("sprites/black_hole/spin" .. index .. ".png"))
	end
end

function BlackHole:update(dt)
	self.currentFrame = self.currentFrame + dt + 0.3

	if self.currentFrame >= 10 then
		self.currentFrame = 1
	end
end

function BlackHole:draw()
	love.graphics.draw(self.frames[math.floor(self.currentFrame)], self.x, self.y)
end