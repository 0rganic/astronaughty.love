Background = Object:extend()

function Background:new()
	self.image = love.graphics.newImage("sprites/background.png")
	self.x = 0
	self.y = 0
	self.width = self.image:getWidth()
	self.height = self.image:getHeight()
end

function Background:draw()
	love.graphics.draw(self.image, self.x, self.y)
end