Player = Object:extend()

function Player:new()
	self.image = love.graphics.newImage("sprites/player/run.png")
	self.danger = love.graphics.newImage("sprites/player/danger.png")
	self.frames = {}
	self.frameWidth = 30
	self.frameHeight = 80
	self.currentFrame = 1
	self.x = 128
	self.y = 256
	self.width = self.image:getWidth()
	self.height = self.image:getHeight()
	self.orientation = 0
	self.scaleFactorX = 1
	self.scaleFactorY = 1
	self.originOffsetX = 15
	self.originOffsetY = 45
	self.shearingFactorX = 0
	self.shearingFactorY = 0
	self.speed = 200
	self.basicAttackCooldown = true
	self.specialAttackCooldown = true
	self.health = 5
	self.dead = false

	for index = 1,4 do
		table.insert(self.frames,
					 love.graphics.newQuad(index * self.frameWidth, 0,
										   self.frameWidth, self.frameHeight,
										   self.width, self.height))
	end

	Tick.recur(function() self.basicAttackCooldown = true end, 1)
	Tick.recur(function() self.specialAttackCooldown = true end, 10)
end

function Player:keyPressed(key)
	if self.basicAttackCooldown then
		if key == "space" then
			table.insert(listOfHearts, Heart(self.x, (self.y - self.height / 2 + 8)))
			self.basicAttackCooldown = false
		end
	end

	if self.specialAttackCooldown then
		if key == "e" and not isActive then
			for heart = 1,30 do
				Tick.delay(function() table.insert(listOfHearts, Heart(self.x, (self.y - self.height / 2 + 8))) end, (heart / 10))
			end

			self.specialAttackCooldown = false
		end
	end

	if key == "escape" then
		love.event.quit()
	end
end

function Player:update(dt)
	-- Player animation
	self.currentFrame = self.currentFrame + 10 * dt
	if self.currentFrame >= 4 then
		self.currentFrame = 1
	end

	-- Horizontal movement
	if love.keyboard.isScancodeDown("left") or love.keyboard.isScancodeDown("a") then
		self.x = self.x - self.speed * dt
	elseif love.keyboard.isScancodeDown("right") or love.keyboard.isScancodeDown("d") then
		self.x = self.x + self.speed * dt
	end
	-- Vertical movement
	if love.keyboard.isScancodeDown("up") or love.keyboard.isScancodeDown("w") then
		self.y = self.y - self.speed * dt
	elseif love.keyboard.isScancodeDown("down") or love.keyboard.isScancodeDown("s") then
		self.y = self.y + self.speed * dt
	end

	-- Getting the dimensions of the game window
	local window_width = love.graphics.getWidth()
	local window_height = love.graphics.getHeight()

	-- Restricting player movement beyond certain thresholds on the horizontal axis
	if self.x < 128 then
		self.x = 128
	elseif self.x > (window_width - self.width / 4) then
		self.x = (window_width - self.width / 4)
	end

	-- Restricting player movement beyond certain thresholds on the vertical axis
	if self.y < (12 + self.height / 2) then
		self.y = (12 + self.height / 2)
	elseif self.y > (window_height - self.height / 2) then
		self.y = (window_height - self.height / 2)
	end
end

function Player:draw()
	love.graphics.draw(self.image, self.frames[math.floor(self.currentFrame)], self.x, self.y, self.orientation, self.scaleFactorX,
					   self.scaleFactorY, self.originOffsetX, self.originOffsetY,
					   self.shearingFactorX, self.shearingFactorY)

	if self.x <= 192 then
		love.graphics.draw(self.danger, self.x - 4, self.y - 70)
	end
end