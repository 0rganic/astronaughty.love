StatusBar = Object:extend()

function StatusBar:new()
	self.font = love.graphics.setNewFont(16)
	self.sign = "<-  Black Hole"
	self.objective = "Objective: 42069"
	self.basicAttackTip = "Press Space to send love"
	self.specialAttackTip = "Press E to spray hearts"
	self.soundVolumeTip = "M to mute the sound"
	self.textPositionX = 96
	self.textpositionY = 64
	self.score = 0
	self.scorePositionX = love.graphics.getWidth() / 2 - 32
	self.scorePositionY = 32
	self.maxScore = 42069
	self.specialAttackCooldown = true
	self.specialAttackCooldownOn = love.graphics.newImage("sprites/heart.png")
	self.specialAttackCooldownOff = love.graphics.newImage("sprites/heart_cooldown.png")
	self.specialAttackCooldownIndicatorX = love.graphics:getWidth() - 96
	self.specialAttackCooldownIndicatorY = 24
end
function StatusBar:update(dt, player)
	self.score = math.floor(self.score + dt + 1)

	if player.specialAttackCooldown then
		self.specialAttackCooldown = true
	else
		self.specialAttackCooldown = false
	end
end

function StatusBar:draw(player)
	love.graphics.print(string.format("%s %s", "HP: ", player.health), 384, 32)

	love.graphics.print(self.score, self.scorePositionX, self.scorePositionY)

	if self.score >= 500 and self.score <= 1000 then
		love.graphics.print(self.basicAttackTip, self.textPositionX, self.textpositionY)
	end

	if self.score <= math.random(0, 400) then
		love.graphics.print(self.sign, self.textPositionX, self.textpositionY)
	end

	if self.score >= 1200 and self.score <= 1700 then
		love.graphics.print(self.specialAttackTip, self.textPositionX, self.textpositionY)
		love.graphics.print(self.soundVolumeTip, self.textPositionX + 8, self.textpositionY + 16)
	end

	if self.score >= 2500 and self.score <= 3500 then
		love.graphics.print(self.objective, self.textPositionX, self.textpositionY)
	end

	if self.specialAttackCooldown then
		love.graphics.draw(self.specialAttackCooldownOn, self.specialAttackCooldownIndicatorX, self.specialAttackCooldownIndicatorY)
	else
		love.graphics.draw(self.specialAttackCooldownOff, self.specialAttackCooldownIndicatorX, self.specialAttackCooldownIndicatorY)
	end

	if player.health <= 0 then
		love.graphics.print("Game Over", 464, 240)
	end

	if self.score >= self.maxScore then
		love.graphics.print("Thanks for playing!", 432, 240)
	end
end