Meteoroid = Object:extend()

function Meteoroid:new()
	self.image = love.graphics.newImage("sprites/meteoroid.png")
	self.x = love.graphics.getWidth()
	self.y = love.math.random(50, 462)
	self.width = self.image:getWidth()
	self.height = self.image:getHeight()
	self.speed = -(love.math.random(1, 1000))
	self.collide = false
	self.destruct = false
end

function Meteoroid:checkCollision(object, soundManager)
	local self_left = self.x
	local self_right = self.x + self.width
	local self_top = self.y
	local self_bottom = self.y + self.height

	local object_left = object.x
	local object_right = object.x
	local object_top = object.y - (object.height / 2) - 4
	local object_bottom = object.y + (object.height / 2) - 8

	if self_right > object_left and
	   self_left < object_right and
	   self_bottom > object_top and
	   self_top < object_bottom then
	   	   object.health = object.health - 1
	   	   soundManager.deathSFX:play()
	       self.collide = true
	       object.collide = true
	end
end

function Meteoroid:selfDestruct(meteoroid, BlackHole)
	if meteoroid.x <= BlackHole.x + 224 then
	   	meteoroid.image = love.graphics.newImage("sprites/collided_meteoroid.png")
		Tick.delay(function() meteoroid.destruct = true end, 0.01)
	end
end

function Meteoroid:update(dt)
	self.x = self.x + self.speed * dt
end

function Meteoroid:draw()
	love.graphics.draw(self.image, self.x, self.y)
end