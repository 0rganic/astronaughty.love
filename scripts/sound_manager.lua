SoundManager = Object:extend()

function SoundManager:new()
	self.theme = love.audio.newSource("music/theme.wav", "stream")
	self.collisionSFX = love.audio.newSource("audio/collision_sfx.wav", "static")
	self.deathSFX = love.audio.newSource("audio/death_sfx.wav", "static")
	self.musicVolume = 0.5
	self.sfxVolume = 0.4
	self.mute = false
	self.theme:setVolume(self.musicVolume)
	self.collisionSFX:setVolume(self.sfxVolume)
	self.deathSFX:setVolume(self.sfxVolume)


	self.theme:setLooping(true)
	self.theme:play()
end

function SoundManager:keyPressed(key)
	if key == "m" and not self.mute then
		self.theme:setVolume(0)
		self.collisionSFX:setVolume(0)
		self.deathSFX:setVolume(0)
		self.mute = true
	elseif key == "m" and self.mute then
		self.theme:setVolume(self.musicVolume)
		self.collisionSFX:setVolume(self.sfxVolume)
		self.deathSFX:setVolume(self.sfxVolume)
		self.mute = false
	end
end