Heart = Object:extend()

function Heart:new(x, y)
	self.image = love.graphics.newImage("sprites/heart.png")
	self.x = x
	self.y = y
	self.speed = 500
	self.width = self.image:getWidth()
	self.height = self.image:getHeight()
	self.collide = false
	self.destruct = false
	self.threshold = love.graphics:getWidth()
end

function Heart:checkCollision(object, soundManager)
	local self_left = self.x
	local self_right = self.x + self.width
	local self_top = self.y
	local self_bottom = self.y + self.height

	local object_left = object.x
	local object_right = object.x + object.width
	local object_top = object.y
	local object_bottom = object.y + object.height

	if self_right > object_left and
	   self_left < object_right and
	   self_bottom > object_top and
	   self_top < object_bottom then
	       soundManager.collisionSFX:play()
	   	   object.image = love.graphics.newImage("sprites/collided_meteoroid.png")
	       self.collide = true
	       object.collide = true
	       statusBar.score = statusBar.score + 100
	end
end

function Heart:selfDestruct(heart)
	if heart.x >= self.threshold then
		heart.destruct = true
	end
end

function Heart:update(dt)
	self.x = self.x + self.speed * dt
end

function Heart:draw()
	love.graphics.draw(self.image, self.x, self.y)
end